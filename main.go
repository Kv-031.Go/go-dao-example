package main

import (
	"log"
	"dao-example/dao/factory"
	"fmt"
	"dao-example/util"
	"dao-example/model"
)

func main()  {
	config, err := util.GetConfiguration()
	if err != nil {
		log.Fatal(err)
		return
	}

	userDao := factory.GetUserDao(config.Engine)
	err = userDao.InitTable()
	if err != nil{
		log.Fatal(err)
		return
	}

	user := model.User{}
	fmt.Println("Enter Firstname:")
	fmt.Scan(&user.FirstName)
	fmt.Println("Enter Lastname:")
	fmt.Scan(&user.LastName)
	fmt.Println("Enter Email:")
	fmt.Scan(&user.Email)

	err = userDao.Create(&user)
	fmt.Println(user)

	if err != nil{
		log.Fatal(err)
		return
	}


}