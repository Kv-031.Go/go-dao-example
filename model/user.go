package model

import "fmt"

type User struct {
	ID        int
	FirstName string
	LastName  string
	Email     string
}

func (user User) String() string{
	return fmt.Sprintf("User object:\n{\n    ID = %d,\n    Firstname = %s,\n    LastName = %s,\n    Email = %s\n}",user.ID, user.FirstName, user.LastName, user.Email)
}
