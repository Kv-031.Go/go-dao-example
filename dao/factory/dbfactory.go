package factory

import (
	"dao-example/dao/interfaces"
	"log"
	"dao-example/dao/mysql"
	"dao-example/dao/postgres"
)

func GetUserDao(e string) interfaces.UserDao {
	var i interfaces.UserDao
	switch e {
	case "mysql":
		i = mysql.UserImplMysql{}
	case "postgres":
		i = postgres.UserImplPostgres{}
	default:
		log.Fatalf("Connection to %s not implemented", e)
		return nil
	}

	return i
}