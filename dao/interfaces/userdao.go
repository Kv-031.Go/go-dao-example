package interfaces

import "dao-example/model"

type UserDao interface {
	Create(u *model.User) error
	/*Update(u *models.User) error
	Delete(i int) error
	GetById(i int) (models.User, error)*/
	GetAll() ([]model.User, error)
	InitTable() error
}

