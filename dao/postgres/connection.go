package postgres

import (
	"database/sql"
	_ "github.com/lib/pq" // Postgres driver, not used directly but through database/sql package
	"log"
	"fmt"
	"dao-example/util"
)

func get() *sql.DB {
	config, err := util.GetConfiguration()
	if err != nil {
		log.Fatalln(err)
		return nil
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", config.User, config.Password, config.Server, config.Port, config.Database)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	return db
}
