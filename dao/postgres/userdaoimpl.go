package postgres

import (
	"dao-example/model"
)

type UserImplPostgres struct {
}

func (dao UserImplPostgres) Create(u *model.User) error {
	query := "INSERT INTO users (first_name, last_name, email) VALUES($1, $2, $3) RETURNING id"
	db := get()
	defer db.Close()
	stmt, err := db.Prepare(query)

	if err != nil {
		return err
	}

	defer stmt.Close()
	var id int
	err = stmt.QueryRow(u.FirstName, u.LastName, u.Email).Scan(&id)
	if err != nil {
		return err
	}

	u.ID = int(id)
	return nil
}

func (dao UserImplPostgres) GetAll() ([]model.User, error) {
	query := "SELECT id, first_name, last_name, email FROM users"
	users := make([]model.User, 0)
	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return users, err
	}

	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return users, err
	}

	for rows.Next() {
		var row model.User
		err := rows.Scan(&row.ID, &row.FirstName, &row.LastName, &row.Email)
		if err != nil {
			return nil, err
		}

		users = append(users, row)
	}

	return users, nil

}

func (dao UserImplPostgres) InitTable() error {
	query := "CREATE TABLE IF NOT EXISTS users (id BIGSERIAL PRIMARY KEY, first_name VARCHAR(45), last_name VARCHAR(45), email VARCHAR(90))"
	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec()
	if err != nil {
		return err
	}
	return nil
}